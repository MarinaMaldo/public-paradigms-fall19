package com.example.shreyakumar.lecture02_practice_fall19;

//import com.example.shreyakumar.lecture02_practice_fall19.utilities.NetworkUtils;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
        import android.util.Log;
        import android.widget.Toast;
        import android.content.Context;
import android.view.Menu;


import com.example.shreyakumar.lecture02_practice_fall19.utilities.NetworkUtils; // our class
import android.os.AsyncTask;
import org.json.JSONArray; // different from org.json.simple
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;


public class MainActivity extends AppCompatActivity {

    // create handles that can be connected to view system elements
    private TextView mSearchResultsTextView;
    private TextView mDisplayTextView;
    private EditText mSearchBoxEditText;
    private Button   mSearchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // R is the class that has all the resources information.

        // connecting handles to view system elements
        mSearchResultsTextView  = (TextView) findViewById(R.id.tv_results);
        mDisplayTextView        = (TextView) findViewById(R.id.tv_display_text);
        mSearchBoxEditText      = (EditText) findViewById(R.id.et_search_box);
        mSearchButton           = (Button)   findViewById(R.id.button_search);

        // detecting and responding to user event of button click
        mSearchButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v){

                        // this code executes when the button is clicked - closure
                        String searchText = mSearchBoxEditText.getText().toString();
                        Log.d("informational", "Search button clicked!" + searchText);
                        mSearchResultsTextView.setText("Searching for " + searchText);

                        Context c = MainActivity.this;
                        String msg = "Search clicked! " + searchText;
                        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();

                        makeSearchQuery();

                                             }   // end of onClick method
                                         } // end of object View.OnClickListener
        ); // end of setOnClickListener invocation

    } // end of onCreate method

    // TODO Function makeSearchQuery
    private void makeSearchQuery(){
        String searchQuery = mSearchBoxEditText.getText().toString();
        mDisplayTextView.setText("Results for " + searchQuery);
        mSearchResultsTextView.setText(""); // empty string

        new FetchNetworkData().execute(searchQuery);

    } // end of method makeSearchQuery

    // TODO new inner async class to do networking
    public class FetchNetworkData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params){
            if (params.length == 0) return null;
            String searchQuery = params[0]; // this will receive whatever is passed to execute

            URL url = NetworkUtils.buildUrl(searchQuery);

            String responseString = null;
            try{
                responseString = NetworkUtils.getResponseFromHttpUrl(url);
            } catch(Exception e){
                e.printStackTrace();
            }
            return responseString;
        }   // end of method doInBackground

        // TODO add onPostExecute
        @Override
        protected void onPostExecute(String responseData){
            String [] titles = processRedditJson(responseData);
            for (String title : titles){
                mSearchResultsTextView.append(title + "\n\n");
            }
        }   // end onPostExecute method

        public String[] processRedditJson(String responseJsonData){
            String[] newsTitles = new String[25];
            try{
                JSONObject allNewsReddit = new JSONObject(responseJsonData);
                JSONObject allNewsObject = allNewsReddit.getJSONObject("data");
                JSONArray children = allNewsObject.getJSONArray("children");
                newsTitles = new String[children.length()];
                for (int i = 0; i < newsTitles.length; i++){
                    JSONObject childJson = children.getJSONObject(i);
                    JSONObject childData = childJson.getJSONObject("data");
                    String title = childData.getString("title");

                    newsTitles[i] = title;
                }   // end of for
            }catch(JSONException e){
                e.printStackTrace();
            }
            return newsTitles;
        }   // end of method processRedditJson


    }   // end of class FetchNetworkData


    // menu inflation and action
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    } // end of onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int menuItemSelected = item.getItemId();
        if(menuItemSelected == R.id.action_about){
            String msg = "About menu item selected!";
            Log.d("informational", msg);
            //Context c = MainActivity.this;
            //Toast.makeText(c, msg, Toast.LENGTH_LONG).show();

            // now let's launch About activity page instead of just the toast
            // TODO grab new message
            //String msg = "news";
            msg = mSearchBoxEditText.getText().toString();
            //Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
            Log.d("informational", msg);

            // TODO call About page
            // create intent and call AboutActivity
            Context c = MainActivity.this;
            Class destinationActivity = AboutActivity.class;

            //intent creation
            Intent startAboutActivityIntent = new Intent(c, destinationActivity);

            startAboutActivityIntent.putExtra(Intent.EXTRA_TEXT, msg);

            startActivity(startAboutActivityIntent);
            Log.d("informational", "About activity launched!");


        }
        return true; // this return value will be used by someone, and we can change to a more meaningful return value
    } // end of onOptionsItemSelected

} // end of class