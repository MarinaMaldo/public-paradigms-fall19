#incomplete code
#movies.py
import cherrypy
import json
from _movie_database import _movie_database()

class MovieController(object):

    def __init__(self, mdb=None):
        if mdb is None:
            self.mdb = _movie_database()
        else:
            self.mdb = mdb

        self.mdb.load_movies('ml-1m/movies.dat')
        self.mdb.load_posters('ml-1m/images.dat') #create this method
        #TODO load other resource files

    def GET_MID(self, movie_id):
        output = {'result' : 'success'}
        movie_id = int(movie_id)

        try:
            movie = self.mdb.get_movie(movie_id)
            if movie is not None:
                output['id']        = movie_id
                output['title']     = movie[0]
                output['genres']    = movie[1]
                output['img']       = self.get_poster_by_mid(movie_id)
            else:
                output['result'] = 'error'
                output['message'] = 'movie not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


    def get_poster_by_mid(self, mid):
        if mid in self.posters.keys():
            return self.posters[mid]
        return '/default.jpg'


    def load_posters(self, posters_file):
        self.posters = {}
        f = open(posters_file)
        for line in f:
            #TODO string parsing to get m_img
            self.posters[mid] = m_img
