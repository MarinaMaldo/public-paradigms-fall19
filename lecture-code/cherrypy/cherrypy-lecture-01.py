import cherrypy
import json

class MyController(object):

    def GET_SIMPLE(self):
        return "boo!"

    def GET_CONDITION(self, condition):
        my_dictionary = {"result" : "success", condition : "boo!"}
        return json.dumps(my_dictionary)

    def PUT_CONDITION(self, condition):
        # extract body of put message
        payload = cherrypy.request.body.read() # string
        print(payload)
        my_dictionary = {"result" : "success", condition : json.loads(payload)}
        return json.dumps(my_dictionary)



def start_service():
    mycon = MyController() # object of controller class

    # create dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    dispatcher.connect('scary_connection', '/halloween/', controller=mycon, action='GET_SIMPLE', conditions=dict(method=['GET']) )
    dispatcher.connect('scary_condition_conn', '/halloween/:condition', controller=mycon, action='GET_CONDITION', conditions=dict(method=['GET']) )
    dispatcher.connect('scary_put_condition_conn', '/halloween/:condition', controller=mycon, action='PUT_CONDITION', conditions=dict(method=['PUT']) )


    # configuration
    conf = {
            'global' : {
                'server.socket_host' : 'student04.cse.nd.edu',
                'server.socket_port' : 51005,
                },
            '/' : { 'request.dispatch' : dispatcher },
            }

    # use config to update and start the server
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    start_service()
