import cherrypy
import json

from dictionary_controller import DictionaryController

def start_service():

    dcon = DictionaryController() # object
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # connect to controller
    dispatcher.connect('dict_get_key', '/dictionary/:key', controller=dcon, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('dict_put_key', '/dictionary/:key', controller=dcon, action='PUT_KEY', conditions=dict(method=['PUT']))

    #configuration
    conf = {
            'global' : {
                'server.socket_host' : 'student04.cse.nd.edu',
                'server.socket_port' : 51005,
                },
            '/' : {'request.dispatch' : dispatcher}
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    start_service()
